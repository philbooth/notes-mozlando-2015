# Mozlando 2015

## Thursday 10th December

### Heka Deep Dive

* about:health-report in your browser shows what telemetry data is being recorded
* heka written in go
  * written to replace logstash, which was painfully slow
* hindsight is successor to heka
  * written in c
  * combines inputs, splitters and decoders into one step
    * can still use same plugins from heka
    * re-use is just at code level rather than framework level
* plugins all written in lua
  * 8kb vs 10mb working set per plugin for js
  * system specifies per sandbox how much memory, cpu etc each filter is allowed
    * beyond those thresholds, filter gets shutdown so the rest of the system is safe
      * can set can_exit to true if filter death is non-fatal
      * otherwise its death will bring down heka
* between each heka plugin is a buffer (heka protobuf stream)
* use heka-cat to cat heka binary messages to console in readable form
  * supports message-matching format so you can pull specific things of interest from s3
  * pulls data from protobuf stream
* inputs, splitters, decoders
  * inputs read data from wherever, http, udp, statsd etc, just raw i/o
  * splitters break data into distinct messages at the appropriate boundary
    * first thing that knows about the data
    * maybe it recognises protobuf headers, maybe newline characters, whatever
    * splitter can also do nothing, if you don't need to split (null splitter)
    * not about dividing individual records, just breaking streams at boundaries
	  * (so not needed for e.g. http requests)
    * splitter examples:
      * null, token, regex, heka framing
  * decoder parses, maps from structured data to heka filter
    * decoder examples:
      * json, protobuf, csv, text, null
* input > splitter > decoder > router (not a plugin) > filter or output+encoder
  * routers have filter or output plugins hanging off them via message matching
    * message matcher defines which messages are the input stream for a filter/output
    * every filter specifies a matcher, every output specifies a matcher
    * matchers can be re-used different plugins
  * filters: messages in, messages out; stream processing
    * can apply transformation, more offten it is some aggregation
  * outputs are almost exactly the same as filters, except they emit data to the outside world
    * (to carbon, elasticsearch, http, udp, file etc)
    * encoder portion converts data to the expected format of the output receiver
* heka instances often chained together (don't have to be)
  * use matchers, outputs etc
* analysis.telemetry.mozilla.org
  * spark and heka jobs, try things out
  * completely self serve, spin up boxes etc
* redshift derived streams most popular place for teams to dump stuff
* lua sandbox
  * lua api knows how to read config
  * pull data from config, then pull data from messages
  * `preserve_data` setting will serialise data to disk when system is restarted, reload it after
    * use it for long-term sliding windows where you don't want heka restarts to clobber data
    * without `preserve_data` being set, data is not persisted
* to debug:
  * build local copy
  * use `inject_message` to spit out any state
  * check dashboard and heka spits it out

